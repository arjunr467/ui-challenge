import 'package:UI/screens/homepage.dart';
import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: "UI",
    theme: ThemeData(
      accentColor: Colors.white,
      primaryColor: Colors.white
    ),
    initialRoute: Homepage.id,
    routes: {
      Homepage.id : (context)=> Homepage(),
    },
  ));
}