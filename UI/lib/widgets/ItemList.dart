import 'package:flutter/material.dart';

class ItemList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 7.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width * 0.47,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Wrap(children: <Widget>[
                      Text(
                        "Super Dinner na 1line",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                          color: Colors.blueGrey[400],
                        ),
                      ),
                    ]),
                    SizedBox(
                      height: 5.0,
                    ),
                    Wrap(
                      children: <Widget>[
                        Text(
                          "4pcs Chicken + Fries + Coleslaw + Bun + Drink in 2 lines",
                          style: TextStyle(color: Colors.grey[600]),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 1.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "KD 2.350",
                          style: TextStyle(
                              color: Colors.orange,
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0)),
                            color: Colors.orange,
                            child: Text(
                              "Add to Cart",
                              style: TextStyle(color: Colors.white),
                            ),
                            onPressed: () {})
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(width: MediaQuery.of(context).size.width * .1),
              Image.asset(
                "lib/images/food.png",
                height: 100,
                width: 100,
                fit: BoxFit.cover,
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Divider(
            color: Colors.grey[400],
            thickness: 0.9,
          )
        ],
      ),
    );
  }
}
