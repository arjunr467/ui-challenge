import 'package:flutter/material.dart';
import 'package:rating_bar/rating_bar.dart';

class RestaurantDetailContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Stack(
          alignment: Alignment.center,
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Image.asset(
              "lib/images/kfc.jpg",
              fit: BoxFit.cover,
              height: MediaQuery.of(context).size.height * .25,
              width: MediaQuery.of(context).size.width,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: MediaQuery.of(context).size.height * .06,
                ),
                Text(
                  "KFC Salmiya KFC Salmia block 12",
                  style: TextStyle(
                      color: Colors.blueGrey[600],
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  "American,Sandwiches,Burgers",
                  style: TextStyle(
                    color: Colors.grey[600],
                    fontSize: 15.0,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RatingBar.readOnly(
                        size: 20.0,
                        isHalfAllowed: true,
                        initialRating: 4.0,
                        filledColor: Colors.yellow[700],
                        emptyColor: Colors.grey[300],
                        filledIcon: Icons.star,
                        halfFilledIcon: Icons.star_half,
                        emptyIcon: Icons.star),
                    SizedBox(
                      width: 5.0,
                    ),
                    Text(
                      "(2 Reviews)",
                      style: TextStyle(
                          color: Colors.grey[600], fontSize: 15.0),
                    )
                  ],
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text("Street number,block number,Area name,Area name"),
                SizedBox(
                  height: 5.0,
                ),
                RichText(
                    textDirection: TextDirection.ltr,
                    textAlign: TextAlign.center,
                    text: TextSpan(children: [
                      TextSpan(
                        text: "Within 30 mins",
                        style: TextStyle(color: Colors.blueGrey[600]),
                      ),
                      TextSpan(
                        text: " (1.000 KWD delivery)",
                        style: TextStyle(color: Colors.grey[600]),
                      )
                    ])),
                SizedBox(
                  height: 10.0,
                ),
                Divider(
                  color: Colors.grey[600],
                )
              ],
            ),
          ],
        ),
        Positioned(
          bottom: 135,
            child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(12.0)),
          child: Padding(
            padding: EdgeInsets.all(7.0),
            child: Image.asset(
              "lib/images/kfc-ico.png",
              height: 50,
              width: 50,
              fit: BoxFit.cover,
            ),
          ),
        ))
      ],
    ));
  }
}
