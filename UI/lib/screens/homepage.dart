import 'package:UI/widgets/ItemList.dart';
import 'package:UI/widgets/RestaurantDetailContainer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Homepage extends StatefulWidget {
  static const String id = "/Home";
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 5,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: IconButton(
              icon: Icon(CupertinoIcons.back),
              color: Colors.grey[600],
              onPressed: () {}),
          title: Text(
            "KFC Salmiyablock 12",
            style: TextStyle(color: Colors.blueGrey[600]),
          ),
          actions: <Widget>[
            IconButton(
                icon: Icon(CupertinoIcons.search),
                color: Colors.grey[600],
                onPressed: () {})
          ],
        ),
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                RestaurantDetailContainer(),
                SizedBox(
                  height: 5.0,
                ),
                TabBar(
                    indicator: UnderlineTabIndicator(
                      borderSide: BorderSide(color: Colors.orange, width: 3.0),
                      insets: EdgeInsets.only(bottom: 11.0),
                    ),
                    indicatorWeight: 3.0,
                    indicatorPadding: EdgeInsets.zero,
                    indicatorColor: Colors.orange,
                    indicatorSize: TabBarIndicatorSize.label,
                    labelColor: Colors.blueGrey[700],
                    isScrollable: true,
                    unselectedLabelColor: Colors.grey[600],
                    tabs: [
                      Tab(
                        text: "Featured",
                      ),
                      Tab(
                        text: "Top selling",
                      ),
                      Tab(
                        text: "All",
                      ),
                      Tab(
                        text: "For One",
                      ),
                      Tab(
                        text: "For Sharing",
                      )
                    ]),
                Container(
                  padding: EdgeInsets.symmetric(horizontal:20.0),
                  width: MediaQuery.of(context).size.width,
                  height: 350,
                  child:
                      TabBarView( children: [
                    Tab(
                      child: ListView.builder(
                        physics: BouncingScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: 8,
                        itemBuilder: (context,index){
                          return ItemList();
                        }
                        ),
                    ),
                    Container(
                      color: Colors.orange,
                    ),
                    Container(
                      color: Colors.black,
                    ),
                    Container(
                      color: Colors.purple,
                    ),
                    Container(
                      color: Colors.teal,
                    ),
                  ]),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

